## Task: Visualization of the task-history
### Description
As a workflow user, I want to see all my open and finished tasks listed with all important data.

### Acceptance criteria:
- I can see a list with open and finished tasks.
- All important data has to be visible.
- The duration of finished tasks should be visualized appropriately.

### Furthers Comments
The developer needs this task done by creating a polymer element, which is easily to include to their polymer application.

#### Tips

For easy access to the camunda server see:

- https://github.com/camunda/docker-camunda-bpm-platform

User/Pass: demo/demo

For the REST API see:

- https://blog.camunda.org/post/2015/08/start-and-complete-process-with-rest-api/
- https://docs.camunda.org/manual/7.6/reference/rest/

For Polymer see:

- https://www.polymer-project.org/1.0/
